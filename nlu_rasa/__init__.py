"""

This is the documentation of the aaambos package Rasa Nlu.
It contains ofaaambos modules, owm run and/or arch configs,  

# About the package

# Background / Literature

# Usage / Examples

## Installation

You can clone and install it:
```bash
conda activate aaambos
pip install 'rasa[spacy]'
git clone https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu.git
python3 -m spacy download de_core_news_sm
rasa telemetry disable
cd aaambos_pkg_rasa_nlu
pip install -e .
```

Everytime you do `pip install -e .` it would unlink local installed (/cloned) aaambos and simple flexdiam installations and install the version from gitlab.
You can prevent it by uncomment the 2 requirements in the setup.py file.

## Running

The config.yml contains the rasa pipeline. You should be able to specify a different one with the `rasa_pipeline_config` paramter for the rasa module config.

The training data folder can also be specified with the `nlu_data_dir` config paramter.

A simple arch_config entry for the nlu_rasa module:
```yaml
modules:
  rasa_nlu:
    module_info: !name:nlu_rasa.modules.rasa_nlu_module.RasaNluModule
```

# Citation


"""
