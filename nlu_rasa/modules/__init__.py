"""
Some example NLU results.

**ich bin Florian Schröder und am 1.1.1950 geboren**
```
NluResults(
 entities=[NluEntity(entity='info_name', value='geburtstag', start=41, end=48, confidence=None, extractor='DIETClassifier'),
           NluEntity(entity='PER', value='bin Florian Schröder', start=4, end=24, confidence=None, extractor='FullSpacyEntityExtractor'),
           NluEntity(entity='MISC', value='1.1.1950', start=32, end=40, confidence=None, extractor='FullSpacyEntityExtractor'),
           NluEntity(entity='date_day', value=1, start=32, end=34, confidence=None, extractor='RegexGroupEntityExtractor'),
           NluEntity(entity='date_month', value=1, start=32, end=33, confidence=None, extractor='RegexGroupEntityExtractor'),
           NluEntity(entity='date_year', value=1950, start=35, end=40, confidence=None, extractor='RegexGroupEntityExtractor'),
           NluEntity(entity='first_name', value='Florian', start=8, end=15, confidence=None, extractor='RegexGroupEntityExtractor'),
           NluEntity(entity='date', value='1-1-1950', start=32, end=40, confidence=None, extractor='RegexGroupEntityExtractor')],
 intention='changeinfo',
 noun_phrases=['ich', 'Florian Schröder', '1.1.1950'],
 state=<AsrResultState.FINAL: 'final'>,
 time=datetime.datetime(2023, 12, 11, 15, 28, 3, 704499),
 tokens=[['ich', 'PRON'],
         ['sein', 'AUX'],
         ['Florian', 'PROPN'],
         ['Schröder', 'PROPN'],
         ['und', 'CCONJ'],
         ['an', 'ADP'],
         ['1.1.1950', 'NOUN'],
         ['gebären', 'VERB']],
 utterance='ich bin Florian Schröder und am 1.1.1950 geboren',
 utterance_id='73e83a28-adb6-4cc1-9d51-f5ba33b60d04'
)
```

**Hobbys sind laufen, spielen, tanzen, singen, lesen, Fußball, Sport, angeln, malen, fotografieren, basteln und werkeln**
```
NluResults(
 entities=[NluEntity(entity='info_name', value='hobbys', start=0, end=6, confidence=None, extractor='DIETClassifier')],
 intention='emotion',
 noun_phrases=['Hobbys', 'Fußball', 'Sport'],
 state=<AsrResultState.FINAL: 'final'>,
 time=datetime.datetime(2023, 12, 11, 15, 31, 6, 906351),
 tokens=[['Hobby', 'NOUN'],
         ['sein', 'AUX'],
         ['laufen', 'VERB'],
         ['--', 'PUNCT'],
         ['spielen', 'VERB'],
         ['--', 'PUNCT'],
         ['tanzen', 'VERB'],
         ['--', 'PUNCT'],
         ['singen', 'VERB'],
         ['--', 'PUNCT'],
         ['lesen', 'VERB'],
         ['--', 'PUNCT'],
         ['Fußball', 'NOUN'],
         ['--', 'PUNCT'],
         ['Sport', 'NOUN'],
         ['--', 'PUNCT'],
         ['angeln', 'VERB'],
         ['--', 'PUNCT'],
         ['malen', 'VERB'],
         ['--', 'PUNCT'],
         ['fotografieren', 'VERB'],
         ['--', 'PUNCT'],
         ['basteln', 'VERB'],
         ['und', 'CCONJ'],
         ['werkeln', 'VERB']],
 utterance='Hobbys sind laufen, spielen, tanzen, singen, lesen, Fußball, '
           'Sport, angeln, malen, fotografieren, basteln und werkeln',
 utterance_id='1f99a443-7767-4183-982d-dbb4d96f30d5'
)
```

**ich arbeite bei Apple mein Chef ist Tim Cook. Ein Freund arbeitet bei Microsoft und ein weiterer bei der Handwerker GmbH**
```
NluResults(
 entities=[NluEntity(entity='ORG', value='Apple', start=16, end=21, confidence=None, extractor='FullSpacyEntityExtractor'),
           NluEntity(entity='PER', value='Tim Cook.', start=36, end=45, confidence=None, extractor='FullSpacyEntityExtractor'),
           NluEntity(entity='ORG', value='Microsoft', start=70, end=79, confidence=None, extractor='FullSpacyEntityExtractor'),
           NluEntity(entity='ORG', value='Handwerker GmbH', start=105, end=120, confidence=None, extractor='FullSpacyEntityExtractor')],
 intention='changeinfo',
 noun_phrases=['ich',
               'Apple',
               'mein Chef',
               'Tim Cook',
               'Ein Freund',
               'Microsoft',
               'der Handwerker GmbH'],
 state=<AsrResultState.FINAL: 'final'>,
 time=datetime.datetime(2023, 12, 11, 15, 33, 17, 704106),
 tokens=[['ich', 'PRON'],
         ['arbeiten', 'VERB'],
         ['bei', 'ADP'],
         ['Apple', 'PROPN'],
         ['mein', 'DET'],
         ['Chef', 'NOUN'],
         ['sein', 'AUX'],
         ['Tim', 'PROPN'],
         ['Cook', 'PROPN'],
         ['--', 'PUNCT'],
         ['ein', 'DET'],
         ['Freund', 'NOUN'],
         ['arbeiten', 'VERB'],
         ['bei', 'ADP'],
         ['Microsoft', 'PROPN'],
         ['und', 'CCONJ'],
         ['ein', 'DET'],
         ['weit', 'ADJ'],
         ['bei', 'ADP'],
         ['der', 'DET'],
         ['Handwerker', 'ADJ'],
         ['GmbH', 'NOUN']],
 utterance='ich arbeite bei Apple mein Chef ist Tim Cook. Ein Freund '
           'arbeitet bei Microsoft und ein weiterer bei der Handwerker GmbH',
 utterance_id='1b3f1e28-eb56-44f9-938c-369ecebf0aed'
)
```

"""