MONTH_WORDS = {
    'januar': 1,
    'january': 1,
    'jan': 1,
    'februar': 2,
    'february': 2,
    'feb': 2,
    'märz': 3,
    'mär': 3,
    'mar': 3,
    'april': 4,
    'apr': 4,
    'abril': 4,
    'abr': 4,
    'mai': 5,
    'juni': 6,
    'jun': 6,
    'june': 6,
    'juli': 7,
    'jul': 7,
    'july': 7,
    'august': 8,
    'aug': 8,
    'september': 9,
    'sep': 9,
    'oktober': 10,
    'okt': 10,
    'october': 10,
    'oct': 10,
    'november': 11,
    'nov': 11,
    'dezember': 12,
    'dez': 12,
    'december': 12,
    'dec': 12,
}


def day_transform(day_str):
    return int(day_str.replace(".", ""))


def month_transform(month_str):
    if month_str.replace(".", "").isdigit():
        return int(month_str.replace(".", ""))
    if month_str.lower() in MONTH_WORDS:
        return MONTH_WORDS[month_str.lower()]
    return -1


def year_transform(year_str):
    if len(year_str) == 3:  # space in front of digit
        return 1900 + int(year_str.replace(".", ""))
    return int(year_str.replace(".", ""))


transformation_map = {
    "first_name": lambda x: x.capitalize(),
    "date_day": day_transform,
    "date_month": month_transform,
    "date_year": year_transform,
}
