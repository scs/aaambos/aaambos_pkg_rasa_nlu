"""
See https://spacy.io/usage/rule-based-matching#dependencymatcher
"""

from spacy.matcher import DependencyMatcher
from spacy.strings import StringStore


def get_matcher(nlu_vocab):
    matcher = DependencyMatcher(nlu_vocab)
    stringstore = StringStore(["MO", "VERB", "PERFECT", "WANT", "PAST", "AUX"])

    perfect_pattern = [
        {
            "RIGHT_ID": "anchor_have",
            "RIGHT_ATTRS": {"LEMMA": "haben"}
        },
        {
            "LEFT_ID": "anchor_have",
            "REL_OP": ">",
            "RIGHT_ID": "connected_verb",
            "RIGHT_ATTRS": {"DEP": "oc"},
        },
    ]
    matcher.add("PERFECT", [perfect_pattern])

    want_pattern = [
        {
            "RIGHT_ID": "anchor_have",
            "RIGHT_ATTRS": {"LEMMA": "wollen"}
        },
        {
            "LEFT_ID": "anchor_have",
            "REL_OP": ">",
            "RIGHT_ID": "connected_verb",
            "RIGHT_ATTRS": {"DEP": "oc"},
        },
    ]
    matcher.add("WANT", [want_pattern])

    no_mo_lemmas = ["auch", "gerne", "warum", "ja", "nein", "sonst", "so", "wie", "was", "wegen", "mit"]
    mo_pattern = [
        {
            "RIGHT_ID": "verb_anchor",
            "RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
        },
        {
            "LEFT_ID": "verb_anchor",
            "REL_OP": ">",
            "RIGHT_ID": "connected_mo",
            "RIGHT_ATTRS": {"DEP": "mo", "LEMMA": {"NOT_IN": no_mo_lemmas}},
        },
    ]
    matcher.add("MO", [mo_pattern])

    verb_pattern = [
        {
            "RIGHT_ID": "past_verb",
            "RIGHT_ATTRS": {"POS": "VERB"},
        },
    ]
    matcher.add("VERB", [verb_pattern])

    aux_pattern = [
        {
            "RIGHT_ID": "past_verb",
            "RIGHT_ATTRS": {"POS": "AUX"},
        },
    ]
    matcher.add("AUX", [aux_pattern])

    verb_past_pattern = [
        {
            "RIGHT_ID": "past_verb",
            "RIGHT_ATTRS": {"POS": "VERB", "MORPH": {"IS_SUPERSET": ["Tense=Past"]}},
        },
    ]
    matcher.add("PAST", [verb_past_pattern])

    pattern_ids = {
        stringstore["PERFECT"]: "PERFECT",
        stringstore["MO"]: "MO",
        stringstore["WANT"]: "WANT",
        stringstore["VERB"]: "VERB",
        stringstore["AUX"]: "AUX",
        stringstore["PAST"]: "PAST",
    }

    return matcher, stringstore, pattern_ids

