from __future__ import annotations
import logging
import re
import traceback
from typing import Any, Dict, List, Optional, Text

from rasa.engine.graph import GraphComponent, ExecutionContext
from rasa.engine.recipes.default_recipe import DefaultV1Recipe
from rasa.engine.storage.storage import ModelStorage
from rasa.engine.storage.resource import Resource
import rasa.shared.utils.io
import rasa.nlu.utils.pattern_utils as pattern_utils
from rasa.shared.nlu.training_data.training_data import TrainingData
from rasa.shared.nlu.training_data.message import Message
from rasa.shared.nlu.constants import (
    ENTITIES,
    ENTITY_ATTRIBUTE_VALUE,
    ENTITY_ATTRIBUTE_START,
    ENTITY_ATTRIBUTE_END,
    TEXT,
    ENTITY_ATTRIBUTE_TYPE,
)
from rasa.nlu.extractors.extractor import EntityExtractorMixin

logger = logging.getLogger(__name__)

# adapted from rasa version 3.0.5.


@DefaultV1Recipe.register(
    DefaultV1Recipe.ComponentType.ENTITY_EXTRACTOR, is_trainable=True
)
class RegexGroupEntityExtractor(GraphComponent, EntityExtractorMixin):
    """Extracts entities via lookup tables and regexes defined in the training data."""

    REGEX_FILE_NAME = "regex.json"

    @staticmethod
    def get_default_config() -> Dict[Text, Any]:
        """The component's default config (see parent class for full docstring)."""
        return {
            # text will be processed with case-insensitive as default
            "case_sensitive": False,
            # use lookup tables to extract entities
            "use_lookup_tables": True,
            # use regexes to extract entities
            "use_regexes": True,
            # use match word boundaries for lookup table
            "use_word_boundaries": True,
            # extract only found regex that are known entities
            "use_only_entities": False,
            # do not add entities if their name start with underscore
            "ignore_start_underscore": True,
            # str: dict to load for transformation step of entities (post_processing): dict with entity name to function / or list of strs
            "entity_transformation": None,
        }

    @classmethod
    def create(
        cls,
        config: Dict[Text, Any],
        model_storage: ModelStorage,
        resource: Resource,
        execution_context: ExecutionContext,
    ) -> RegexGroupEntityExtractor:
        """Creates a new `GraphComponent`.

        Args:
            config: This config overrides the `default_config`.
            model_storage: Storage which graph components can use to persist and load
                themselves.
            resource: Resource locator for this component which can be used to persist
                and load itself from the `model_storage`.
            execution_context: Information about the current graph run. Unused.

        Returns: An instantiated `GraphComponent`.
        """
        return cls(config, model_storage, resource)

    def __init__(
        self,
        config: Dict[Text, Any],
        model_storage: ModelStorage,
        resource: Resource,
        patterns: Optional[List[Dict[Text, Any]]] = None,
    ) -> None:
        """Creates a new instance.

        Args:
            config: The configuration.
            model_storage: Storage which graph components can use to persist and load
                themselves.
            resource: Resource locator for this component which can be used to persist
                and load itself from the `model_storage`.
            patterns: a list of patterns
        """
        # graph component
        self._config = {**self.get_default_config(), **config}
        self._model_storage = model_storage
        self._resource = resource
        # extractor
        self.case_sensitive = self._config["case_sensitive"]
        self.ignore_underscore = self._config["ignore_start_underscore"]
        self.patterns = patterns or []
        self.entity_transformation = {}

        if self._config["entity_transformation"]:
            import importlib
            try:

                if isinstance(self._config["entity_transformation"], list):
                    transformation_modules = self._config["entity_transformation"]
                else:
                    transformation_modules = [self._config["entity_transformation"]]

                for transform_info in transformation_modules:
                    mod, _, class_name = transform_info.rpartition(".")
                    loaded_module = importlib.import_module(mod)
                    self.entity_transformation.update(loaded_module.__dict__[class_name])
                    del loaded_module
            except Exception:
                print(traceback.format_exc())

        self._adapt_patterns()

    def train(self, training_data: TrainingData) -> Resource:
        """Extract patterns from the training data.

        Args:
            training_data: the training data
        """
        self.patterns = pattern_utils.extract_patterns(
            training_data,
            use_lookup_tables=self._config["use_lookup_tables"],
            use_regexes=self._config["use_regexes"],
            use_only_entities=self._config["use_only_entities"],
            use_word_boundaries=self._config["use_word_boundaries"],
        )

        if not self.patterns:
            rasa.shared.utils.io.raise_warning(
                "No lookup tables or regexes defined in the training data that have "
                "a name equal to any entity in the training data. In order for this "
                "component to work you need to define valid lookup tables or regexes "
                "in the training data."
            )
        self.persist()
        return self._resource

    def process(self, messages: List[Message]) -> List[Message]:
        """Extracts entities from messages and appends them to the attribute.

        If no patterns where found during training, then the given messages will not
        be modified. In particular, if no `ENTITIES` attribute exists yet, then
        it will *not* be created.

        If no pattern can be found in the given message, then no entities will be
        added to any existing list of entities. However, if no `ENTITIES` attribute
        exists yet, then an `ENTITIES` attribute will be created.

        Returns:
           the given list of messages that have been modified
        """
        if not self.patterns:
            rasa.shared.utils.io.raise_warning(
                f"The {self.__class__.__name__} has not been "
                f"trained properly yet. "
                f"Continuing without extracting entities via this extractor."
            )
            return messages

        for message in messages:
            extracted_entities = self._extract_entities(message)
            extracted_entities = self.add_extractor_name(extracted_entities)
            message.set(
                ENTITIES,
                message.get(ENTITIES, []) + extracted_entities,
                add_to_output=True,
            )
        return messages

    def _extract_entities(self, message: Message) -> List[Dict[Text, Any]]:
        """Extract entities of the given type from the given user message.

        Args:
            message: a message
        Returns:
            a list of dictionaries describing the entities
        """
        message_text = message.get(TEXT)
        entities = []

        for pattern in self.patterns:
            for match in pattern["re"].finditer(message_text):
                match_text = match.group()
                start_index = match.start()
                end_index = match.end()
                for group_name in pattern["re"].groupindex:
                    group_match = match.group(group_name)
                    if group_match:
                        start_group_index = start_index + match_text.index(group_match)
                        entities.append(
                            {
                                ENTITY_ATTRIBUTE_TYPE: group_name,
                                ENTITY_ATTRIBUTE_START: start_group_index,
                                ENTITY_ATTRIBUTE_END: start_group_index + len(group_match),
                                ENTITY_ATTRIBUTE_VALUE: self._transform_entity(group_name, group_match),
                            }
                        )
                pattern_name = pattern["name"]
                if not self.ignore_underscore or not pattern_name[0] == "_":
                    entities.append(
                        {
                            ENTITY_ATTRIBUTE_TYPE: pattern_name,
                            ENTITY_ATTRIBUTE_START: start_index,
                            ENTITY_ATTRIBUTE_END: end_index,
                            ENTITY_ATTRIBUTE_VALUE: self._transform_entity(pattern_name, match_text),
                        }
                    )

        return entities

    @classmethod
    def load(
        cls,
        config: Dict[Text, Any],
        model_storage: ModelStorage,
        resource: Resource,
        execution_context: ExecutionContext,
        **kwargs: Any,
    ) -> RegexGroupEntityExtractor:
        """Loads trained component (see parent class for full docstring)."""
        try:
            with model_storage.read_from(resource) as model_path:
                regex_file = model_path / cls.REGEX_FILE_NAME
                patterns = rasa.shared.utils.io.read_json_file(regex_file)
                return cls(
                    config,
                    model_storage=model_storage,
                    resource=resource,
                    patterns=patterns,
                )
        except (ValueError, FileNotFoundError):
            rasa.shared.utils.io.raise_warning(
                f"Failed to load {cls.__name__} from model storage. "
                f"This can happen if the model could not be trained because regexes "
                f"could not be extracted from the given training data - and hence "
                f"could not be persisted."
            )
            return cls(config, model_storage=model_storage, resource=resource)

    def persist(self) -> None:
        """Persist this model."""
        if not self.patterns:
            return
        with self._model_storage.write_to(self._resource) as model_path:
            regex_file = model_path / self.REGEX_FILE_NAME
            keys_to_store = ["pattern", "name"]
            store_patterns = [{key: pattern[key] for key in keys_to_store} for pattern in self.patterns]
            rasa.shared.utils.io.dump_obj_as_json_to_file(regex_file, store_patterns)

    def _adapt_patterns(self):
        flags = 0 if self.case_sensitive else re.IGNORECASE
        for pattern in self.patterns:
            pattern["re"] = re.compile(pattern["pattern"], flags=flags)

    def _transform_entity(self, entity_name, entity_value):
        try:
            if entity_name in self.entity_transformation:
                return self.entity_transformation[entity_name](entity_value)
        except Exception:
            print(traceback.format_exc())
        return entity_value
