from typing import Dict, Text, Any, List, Type

from rasa.engine.graph import GraphComponent, ExecutionContext
from rasa.engine.recipes.default_recipe import DefaultV1Recipe
from rasa.engine.storage.resource import Resource
from rasa.engine.storage.storage import ModelStorage
from rasa.shared.nlu.training_data.message import Message


from nlu_rasa.modules.custom_rasa_components.dependency_patterns import get_matcher
from nlu_rasa.modules.custom_rasa_components.full_spacy_tokenizer import FullSpacyTokenizer


@DefaultV1Recipe.register(
    DefaultV1Recipe.ComponentType.ENTITY_EXTRACTOR,
    is_trainable=False,
)
class SpacyDependencyMatcher(GraphComponent):
    """ Matching of Dependencies defined in dependency_patterns.py and store them with the key 'dependency_matches' inside the message. """

    @classmethod
    def required_components(cls) -> List[Type]:
        """Components that should be included in the pipeline before this component."""
        return [FullSpacyTokenizer]

    @staticmethod
    def get_default_config() -> Dict[Text, Any]:
        """The component's default config (see parent class for full docstring)."""
        return {}

    def __init__(self, config: Dict[Text, Any]) -> None:
        """Initialize SpacyEntityExtractor."""
        self._config = config
        self.matcher, self.string_store, self.pattern_ids = None, None, None

    @classmethod
    def create(
        cls,
        config: Dict[Text, Any],
        model_storage: ModelStorage,
        resource: Resource,
        execution_context: ExecutionContext,
    ) -> GraphComponent:
        return cls(config)

    def process(self, messages: List[Message]) -> List[Message]:
        if self.matcher is None:
            if not messages:
                return messages
            doc = messages[0].get('text_spacy_doc')
            self.matcher, self.string_store, self.pattern_ids = get_matcher(doc.vocab)

        for message in messages:
            matches = {}
            for match_id, token_ids in self.matcher(message.get('text_spacy_doc')):
                if self.pattern_ids[match_id] in matches:
                    matches[self.pattern_ids[match_id]].append(token_ids)
                else:
                    matches[self.pattern_ids[match_id]] = [token_ids]
            message.set(
                "dependency_matches",
                matches,
                add_to_output=True
            )
        return messages
