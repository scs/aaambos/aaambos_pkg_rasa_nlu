import typing
from typing import Dict, Text, List, Any, Optional, Type

from rasa.engine.recipes.default_recipe import DefaultV1Recipe
from rasa.nlu.tokenizers.tokenizer import Token, Tokenizer
from rasa.nlu.constants import SPACY_DOCS
from rasa.shared.nlu.training_data.message import Message

from nlu_rasa.modules.custom_rasa_components.full_spacy_utils import FullSpacyNLP

if typing.TYPE_CHECKING:
    from spacy.tokens.doc import Doc

POS_TAG_KEY = "pos"

# adapted from rasa version 3.0.5.


@DefaultV1Recipe.register(
    DefaultV1Recipe.ComponentType.MESSAGE_TOKENIZER, is_trainable=False
)
class FullSpacyTokenizer(Tokenizer):
    """Tokenizer that uses SpaCy."""

    @classmethod
    def required_components(cls) -> List[Type]:
        """Components that should be included in the pipeline before this component."""
        return [FullSpacyNLP]

    @staticmethod
    def get_default_config() -> Dict[Text, Any]:
        """The component's default config (see parent class for full docstring)."""
        return {
            # Flag to check whether to split intents
            "intent_tokenization_flag": False,
            # Symbol on which intent should be split
            "intent_split_symbol": "_",
            # Regular expression to detect tokens
            "token_pattern": None,
        }

    @staticmethod
    def _get_doc(message: Message, attribute: Text) -> Optional["Doc"]:
        return message.get(SPACY_DOCS[attribute])

    def tokenize(self, message: Message, attribute: Text) -> List[Token]:
        """Tokenizes the text of the provided attribute of the incoming message."""
        doc = self._get_doc(message, attribute)
        if not doc:
            return []

        tokens = [
            Token(
                t.text, t.idx, lemma=t.lemma_, data={POS_TAG_KEY: t.tag_}
            )
            for t in doc
            if t.text and t.text.strip()
        ]

        message.set(
            "spacy_tokens",
            [(t.lemma_, t.pos_) for t in doc if t.text and t.text.strip()],
            add_to_output=True,
        )

        message.set(
            "spacy_noun_chunks",
            [chunk.text for chunk in doc.noun_chunks],
            add_to_output=True,
        )

        return self._apply_token_pattern(tokens)
