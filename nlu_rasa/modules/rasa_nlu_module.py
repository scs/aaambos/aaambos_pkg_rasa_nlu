from __future__ import annotations

import datetime
import os
from pathlib import Path
from typing import Type

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from rasa.core.channels import UserMessage
from rasa.core.processor import MessageProcessor
from rasa.engine.constants import PLACEHOLDER_MESSAGE
from rasa.model_training import train_nlu
from simple_flexdiam.modules.core.definitions import AsrResults, \
    NluResultsComFeatureIn, NluResultsComFeatureOut, AsrResultsComFeatureOut, AsrResultsComFeatureIn, NluResults, \
    NluEntity, ASR_RESULTS, NLU_RESULTS

MODELS_DIR = "models"
MODEL_NAME = "RasaNLUModel"

NLU_ENTITY_KEYS = {"entity", "value", "start", "end", "confidence", "extractor"}


class RasaNluModule(Module, ModuleInfo):
    config: RasaNluModuleConfig
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
    model_metadata = None
    graph_runner = None

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            NluResultsComFeatureOut.name: (NluResultsComFeatureOut, SimpleFeatureNecessity.Required),
            AsrResultsComFeatureIn.name: (AsrResultsComFeatureIn, SimpleFeatureNecessity.Required)
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            NluResultsComFeatureIn.name: (NluResultsComFeatureIn, SimpleFeatureNecessity.Required),
            AsrResultsComFeatureOut.name: (AsrResultsComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return RasaNluModuleConfig

    async def initialize(self):
        this_modules_dir = str(os.path.dirname(os.path.realpath(__file__)))
        if self.config.rasa_pipeline_config:
            rasa_pipeline_config = str(os.path.expanduser(self.config.rasa_pipeline_config))
        else:
            rasa_pipeline_config = this_modules_dir + "/../configs/config.yml"
        self.log.info(f"Using rasa pipeline config: {rasa_pipeline_config}")
        if self.config.nlu_data_dir:
            nlu_data_dir = str(os.path.expanduser(self.config.nlu_data_dir))
        else:
            nlu_data_dir = this_modules_dir + "/../training_data"
        self.log.info(f"Using training data in dir: {nlu_data_dir}")


        model_output = Path(os.path.expanduser(self.config.general_run_config.local_agent_directories), "models")

        if self.config.training:
            model_path = train_nlu(
                config=rasa_pipeline_config,
                nlu_data=nlu_data_dir,
                output=str(model_output),
                fixed_model_name=MODEL_NAME
            )
        else:
            model_path = model_output

        _, self.model_metadata, self.graph_runner = MessageProcessor._load_model(model_path)

        await self.com.register_callback_for_promise(self.prm[ASR_RESULTS], self.handle_asr_result)

    async def handle_asr_result(self, topic, asr_result: AsrResults):
        message = UserMessage(asr_result.best)

        results = self.graph_runner.run(
            inputs={PLACEHOLDER_MESSAGE: [message]},
            targets=[self.model_metadata.nlu_target],
        )

        await self.com.send(self.tpc[NLU_RESULTS], self._convert_for_msg(self._postprocess_parser_result(results[self.model_metadata.nlu_target][0].as_dict(True)),asr_result))

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code

    @staticmethod
    def _postprocess_parser_result(parser_result):
        """ Add grouped entities, e.g., date entity from day,month,year entities. """
        for i in range(len(parser_result["entities"])):
            if parser_result["entities"][i]["entity"] in ["info_name"]:
                parser_result["entities"][i]["value"] = parser_result["entities"][i]["value"].lower()

        parser_entities = {entity["entity"]: entity for entity in parser_result["entities"]}

        if "date_day" in parser_entities:
            date_string = str(parser_entities["date_day"]["value"]) + "-" + \
                          (str(parser_entities["date_month"]["value"]) if "date_month" in parser_entities else "XX") + "-" + \
                          (str(parser_entities["date_year"]["value"]) if "date_year" in parser_entities else "XXXX")
            end = max(parser_entities["date_day"]["end"], parser_entities["date_month"]["end"] if "date_month" in parser_entities else -1, parser_entities["date_year"]["end"] if "date_year" in parser_entities else -1)
            parser_result["entities"].append({"entity": "date", "value": date_string, "extractor": parser_entities["date_day"]["extractor"],"start": parser_entities["date_day"]["start"], "end": end})

        return parser_result

    @staticmethod
    def _convert_for_msg(rasa_result, asr_result: AsrResults):
        """ Convert parser results to the used keys and properties from Flexdiam."""
        return NluResults(
            intention=rasa_result["intent"]["name"] if rasa_result["intent"]["name"] else "",
            utterance=str(rasa_result["text"]),
            entities=[NluEntity(**{e:v for e, v in ent.items() if e in NLU_ENTITY_KEYS}) for ent in rasa_result["entities"]],
            state=asr_result.state,
            tokens=rasa_result["spacy_tokens"],
            noun_phrases=rasa_result["spacy_noun_chunks"],
            utterance_id=asr_result.utterance_id,
            # spacy_dependency_matches= rasa_result["dependency_matches"],
            time=datetime.datetime.now(),
        )


@define(kw_only=True)
class RasaNluModuleConfig(ModuleConfig):
    module_path: str = "nlu_rasa.modules.rasa_nlu_module"
    module_info: Type[ModuleInfo] = RasaNluModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    training: bool = True
    rasa_pipeline_config: str = ""
    nlu_data_dir: str = ""
    models_path: str = "models/rasa/"


def provide_module():
    return RasaNluModule
