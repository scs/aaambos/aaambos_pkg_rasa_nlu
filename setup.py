#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = [
    "aaambos @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main",
    "simple_flexdiam @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam@main",
]

test_requirements = ['pytest>=3', ]

setup(
    author="Florian Schröder",
    author_email='florian.schroeder@uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="A Natural Language Understanding Module based on Rasa",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    keywords=['aaambos', 'nlu_rasa'],
    name='nlu_rasa',
    packages=find_packages(include=['nlu_rasa', 'nlu_rasa.*']),
    package_data={'': ['configs/*', 'training_data/*']},
    include_package_data=True,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu',
    version='0.1.3',
    zip_safe=False,
)