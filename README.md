# Rasa Nlu

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_rasa_nlu)

> This is an [AAAMBOS](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/) package. You can find more information about what AAAMBOS is [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

A Natural Language Understanding Module based on Rasa

## Install

You can clone and install it:
```bash
conda activate aaambos
pip install 'rasa[spacy]'
git clone https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu.git
python3 -m spacy download de_core_news_sm
rasa telemetry disable
cd aaambos_pkg_rasa_nlu
pip install -e .
```

Everytime you do `pip install -e .` it would unlink local installed (/cloned) aaambos and simple flexdiam installations and install the version from gitlab.
You can prevent it by uncomment the 2 requirements in the setup.py file.

If `python3 -m spacy download de_core_news_sm` does not work (mac user) then

1. Wheel herunterladen: https://github.com/explosion/spacy-models/releases/tag/de_core_news_sm-3.7.0
2. `pip install ~/Downloads/de_core_news_sm-3.7.0-py3-none-any.whl`

## Running

The config.yml contains the rasa pipeline. You should be able to specify a different one with the `rasa_pipeline_config` parameter for the rasa module config.

The training data folder can also be specified with the `nlu_data_dir` config parameter.

A simple arch_config entry for the nlu_rasa module:
```yaml
modules:
  rasa_nlu:
    module_info: !name:nlu_rasa.modules.rasa_nlu_module.RasaNluModule
```

# Flair Nlu

There is also a simpler version with [flair nlp](https://flairnlp.github.io/). It just does the intent recognition with the same training data file but with the advantage of faster starting time compared to rasa.
It is on the `flair_nlp` branch. Maybe in the future, additional features like regex and entities will be added.

It is also much easier to install:
```bash
conda activate aaambos
pip install nlu_flair@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu@flair_nlp
```



